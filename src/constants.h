/******************************************************************************
* openflap
* Copyright (C) 2022  Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Includes
#include <stdint.h>
#include <string>

//     Config
const  int          DEFAULT_SCREEN_WIDTH           = 800;
const  int          DEFAULT_SCREEN_HEIGHT          = 600;
const  int          DEFAULT_VSYNC                  = 1;
const  bool         DEFAULT_FULLSCREEN             = false;
const  bool         DEFAULT_AUDIOENABLED           = true;
const  double       DEFAULT_TIMESTEP               = 1.0/100.0;
const  float        DEFAULT_SOUNDVOLUME            = 1.0f;
const  float        DEFAULT_MUSICVOLUME            = 0.8f;

//     Game
const  std::string  GAME_WINDOWTITLE               = "openflap";
const  float        GAME_JUMP_POWER                = -670.0f;
const  float        GAME_GRAVITY                   = 1600.0f;
const  float        GAME_WALL_VELOCITY             = -210.0f;
const  float        GAME_WALL_WIDTH                = 100.0f;
const  float        GAME_WALL_SPACING              = 105.0f;
const  float        GAME_SPAWN_RANGE               = 145.0f;
const  double       GAME_SPAWNTIME                 = 1.6;
const  double       GAME_DIED_WAIT_TIME            = 0.3;
