execute_process(COMMAND bash -c "set -o pipefail; git log --oneline | wc -l" RESULT_VARIABLE RETURN_CODE OUTPUT_VARIABLE BUILD_NUMBER OUTPUT_STRIP_TRAILING_WHITESPACE)

if(RETURN_CODE EQUAL 0)
	configure_file(
		"${SRC}"
		"${DST}"
		@ONLY
	)
endif()
