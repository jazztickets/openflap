#!/bin/bash

# setup
mkdir -p out

# variables
project=openflap
version=$(grep 'GAME_VERSION=".*"' -o ../CMakeLists.txt | sed -r "s/GAME_VERSION=\"(.*)\"/\1/")
gitver=$(git log --oneline | wc -l)
base=${project}-${version}r${gitver}
pkg=${base}-src.tar.gz

# build package
tar --transform "s,^,$base/," -czvf "out/$pkg" -C ../ \
--exclude="$pkg" \
--exclude=*.swp \
--exclude=.git \
--exclude=working/"$project"* \
--exclude=deployment/out \
--exclude=deployment/*.sh \
src/ \
working/ \
deployment/openflap{,.desktop,.png,.xml} \
cmake/ \
CMakeLists.txt \
build.sh \
README \
LICENSE

echo -e "\nMade $pkg"
