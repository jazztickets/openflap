#!/bin/bash

# get parameters
project=openflap

# get base project dir
projectdir=$(git rev-parse --show-toplevel)
if [ -z "$projectdir" ]; then
	echo "No .git directory found"
	exit 1
fi

# build out dir
outputdir="$projectdir/deployment/out"
mkdir -p "$outputdir"

# get versions
version=$(grep 'GAME_VERSION=".*"' -o "$projectdir"/CMakeLists.txt | sed -r "s/GAME_VERSION=\"(.*)\"/\1/")
gitver=$(git log --oneline | wc -l)

build() {

	bits=$1

	# get mingw prefix
	if [ "$bits" -eq "32" ]; then
		arch=i686-w64-mingw32
	else
		arch=x86_64-w64-mingw32
	fi

	# run cmake
	builddir="$projectdir/build/mingw$bits"
	mkdir -p "$builddir"
	pushd "$builddir" || exit
	cmake -GNinja -DCMAKE_TOOLCHAIN_FILE="../../cmake/mingw${bits}.cmake" ../../

	# build
	if ! ninja; then
		echo "failed $builddir"
		exit
	fi

	# go back to deployment dir
	popd || exit

	# create new working dir
	archive_base=${project}-${version}r${gitver}-win${bits}
	archive=${archive_base}.zip
	rm -rf "${archive_base}"
	cp -r "${projectdir}/working" "${archive_base}"
	rm "${projectdir}/working/${project}.exe"

	# remove linux only files
	rm -f "${archive_base}"/"${project}"{,_debug}

	# copy dlls
	cp /usr/$arch/bin/{libssp-0.dll,libgcc_*.dll,libstdc++-6.dll,libwinpthread-1.dll,SDL2.dll,SDL2_image.dll,SDL2_ttf.dll,SDL2_mixer.dll,libvorbisfile-3.dll,libvorbis-0.dll,libogg.dll,libpng16-16.dll,zlib1.dll} "${archive_base}"/

	# strip exe
	${arch}-strip "${archive_base}"/${project}.exe

	# copy files
	cp "${projectdir}"/README "${archive_base}"/
	echo "${project}.exe 123" > "$archive_base/static_seed.bat"
	chmod +x "${archive_base}"/*.bat

	# zip
	zip -r "${archive}" "${archive_base}"

	# clean up
	rm -rf "${archive_base}"
	mv "$archive" "$outputdir"
}

# remove old zips
rm -f "$outputdir"/"${project}-${version}"*.zip

# build projects
#build 32
build 64
